"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import flask
import os
import app
from flask import Flask, redirect, url_for, request, render_template, jsonify
from pymongo import MongoClient
import arrow
import acp_times
import config
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


app = Flask(__name__)
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb


###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('miles', 999, type=int)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km


    distance = request.args.get('brevet_disk_km', 999, type = int)
    date = request.args.get('begin_date')
    time = request.args.get('begin_time')


    time_combine = str(date) + "T" + str(time) 
    
    re_time = arrow.get(time_combine)
    re_time = re_time.isoformat()
    
    open_time = acp_times.open_time(km, distance, re_time)
    close_time = acp_times.close_time(km, distance, re_time)
    
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############
@app.route('/error')
def error():
    return render_template('error.html')

@app.route('/todo')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    db.tododb.drop()
    dictionary = request.args.get('data')
    # if dictionary == "":
    #     return 
    distance = request.args.get('brevet_dis')
    distance_doc = {'distance':distance}
    db.tododb.insert_one(distance_doc)
    result = dictionary.split(',')
    for i in range(0,len(result),4):
        new_dic = {}
        new_dic['km'] = result[i]
        new_dic['open'] = result[i+1]
        new_dic['close'] = result[i+2]
        db.tododb.insert_one(new_dic)
    return flask.jsonify()


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
